#!/usr/bin/env python3

import click
import random

from math import sqrt
from tabulate import tabulate


def board(seed=None, mode="4"):
    smol_dice = [
        ["C", "A", "P", "S", "H", "O"],
        ["W", "O", "O", "T", "A", "T"],
        ["S", "I", "U", "N", "E", "E"],
        ["I", "D", "E", "X", "L", "R"],
        ["L", "R", "D", "Y", "V", "E"],
        ["Y", "T", "E", "T", "R", "L"],
        ["V", "T", "W", "E", "R", "H"],
        ["T", "I", "Y", "S", "D", "T"],
        ["M", "I", "C", "O", "U", "T"],
        ["U", "Qu", "I", "M", "N", "H"],
        ["G", "H", "N", "E", "E", "W"],
        ["H", "Z", "N", "N", "R", "L"],
        ["J", "O", "B", "B", "A", "O"],
        ["G", "A", "N", "E", "A", "E"],
        ["S", "E", "S", "O", "T", "I"],
        ["K", "F", "A", "P", "S", "F"],
    ]
    big_dice = [
        ['A','A','A','F','R','S'],
        ['A','A','E','E','E','E'],
        ['A','A','F','I','R','S'],
        ['A','D','E','N','N','N'],
        ['A','E','E','E','E','M'],
        ['A','E','E','G','M','U'],
        ['A','E','G','M','N','N'],
        ['A','F','I','R','S','Y'],
        ['B','B','J','K','X','Z'],
        ['C','C','E','N','S','T'],
        ['E','I','I','L','S','T'],
        ['C','E','I','P','S','T'],
        ['D','D','H','N','O','T'],
        ['D','H','H','L','O','R'],
        ['D','H','H','N','O','W'],
        ['D','H','L','N','O','R'],
        ['E','I','I','I','T','T'],
        ['E','I','L','P','S','T'],
        ['E','M','O','T','T','T'],
        ['E','N','S','S','S','U'],
        ['He','Qu','In','Th','Er','An'],
        ['G','O','R','R','V','W'],
        ['I','P','R','S','Y','Y'],
        ['N','O','O','T','U','W'],
        ['O','O','O','T','T','U']
    ]
    dice = smol_dice if mode == "4" else big_dice
    random.seed(seed) if seed is not None else random.seed()
    chosen = [random.choice(die) for die in dice]
    random.shuffle(chosen)
    return chosen


@click.command()
@click.option("--seed", "-s", help="A key to create a board from", default=None)
@click.option(
    "--mode",
    "-m",
    help="Size of the board",
    default="4",
    type=click.Choice(["4", "5"], case_sensitive=False),
)
def board_cli(seed, mode):
    chosen = board(seed, mode)
    size = int(sqrt(len(chosen)))
    table = [chosen[i * size : (i + 1) * size] for i in range(size)]
    click.echo(tabulate(table, tablefmt="fancy_grid"))


if __name__ == "__main__":
    board_cli()
