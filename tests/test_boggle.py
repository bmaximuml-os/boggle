from boggle import board


def test_boggle_random(letter_list):
    b = board()
    assert len(b) == 16
    assert all([letter in letter_list for letter in b]) is True


def test_boggle_fish(letter_list):
    b = board("fish")
    assert len(b) == 16
    assert all([letter in letter_list for letter in b]) is True
    assert b == [
        "T",
        "F",
        "R",
        "O",
        "E",
        "C",
        "E",
        "W",
        "H",
        "E",
        "J",
        "Y",
        "T",
        "N",
        "V",
        "W",
    ]


def test_boggle_1234(letter_list):
    b = board("1234")
    assert len(b) == 16
    assert all([letter in letter_list for letter in b]) is True
    assert b == [
        "E",
        "V",
        "O",
        "A",
        "T",
        "T",
        "R",
        "L",
        "A",
        "M",
        "H",
        "E",
        "E",
        "F",
        "O",
        "E",
    ]


def test_boggle_symbols(letter_list):
    b = board('<@?!"*&^%')
    assert len(b) == 16
    assert all([letter in letter_list for letter in b]) is True
    assert b == [
        "E",
        "D",
        "W",
        "E",
        "A",
        "X",
        "S",
        "D",
        "O",
        "U",
        "N",
        "H",
        "J",
        "T",
        "N",
        "I",
    ]

def test_biggle(letter_list):
    b = board('embiggened', "5")
    assert len(b) == 25
    assert b == [
        "An",
        "S",
        "S",
        "S",
        "T",
        "T",
        "I",
        "H",
        "E",
        "T",
        "U",
        "L",
        "H",
        "G",
        "L",
        "O",
        "P",
        "N",
        "V",
        "U",
        "E",
        "S",
        "E",
        "M",
        "B"
    ]